from scrapy import Spider
from scrapy.selector import Selector

from base.items import BeercrawlItem

class bierdeluxe( Spider ):
    name = "bierdeluxe"
    allowed_domains = ["bier-deluxe.de"]
    start_urls = ['http://www.bier-deluxe.de/sortiment/?p={}'.format(page) for page in range(40)]


    def parse(self, response):
        beers = Selector(response).xpath('//a[@class="product--title"]')

        for beer in beers:
            item = BeercrawlItem()
            item['title'] = beer.xpath('@title').extract()[0]
            yield item