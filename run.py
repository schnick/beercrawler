#!/usr/bin/python
from scrapy.crawler import CrawlerProcess

import base.spiders.bierdeluxe as bdc

if __name__ == '__main__':
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })

    process.crawl(bdc.bierdeluxe)
    #start the actual crawling
    process.start() # the script will block here until the crawling is finished